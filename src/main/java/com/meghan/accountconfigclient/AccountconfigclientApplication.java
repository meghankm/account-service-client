package com.meghan.accountconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@SpringBootApplication
public class AccountconfigclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountconfigclientApplication.class, args);
	}

}
